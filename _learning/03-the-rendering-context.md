---
title: "The Html5 Canvas Element Rendering Context"
permalink: /learning/canvas-rendering-context/
excerpt: "The canvas element rendering context."
description: "The Html5 canvas element rendering context."
last_modified_at: 2021-09-19T21:25:30+03:00
toc: true
---

## The Canvas Rendering Context

The canvas element creates a **rendering surface** that exposes one or more **rendering contexts**. Rendering contexts are used for drawing shapes, text, images, and other objects. Examples of rendering contexts are **2D** rendering context and **3D** rendering context which is used by **WebGL**.

In this tutorial, we will focus on the 2D rendering context. To get the 2D context of the canvas, you call getContext of the canvas element with `'2d'` context type parameter. The `2d` context type API is implemented using the `CanvasRenderingContext2D` interface.

Here is the snippet for getting the context:

```js
var canvas = document.getElementById('canvas'); 
var context = canvas.getContext('2d');  
```

In the first line, the `<canvas>` element is retrieved by calling the `document.getElementById()` method. In the second line, the 2D drawing context is retrieved by using canvas element't `getContext()` method. The context will be used for drawing and manipulating images and graphics on the canvas element.

Each canvas has its own context. If the html page contains more than one canvas elements; you must get each individual context of the canvas that you want to work with.

```js
var canvas1 = document.getElementById('canvas1'); 
var canvas2 = document.getElementById('canvas2'); 
var context1 = canvas.getContext('2d');  
var context2 = canvas.getContext('2d');  
```

Canvas 2D rendering context has many methods and properties that are implemented in the canvas 2D API. Some of the notable ones are listed below:

### State

* **save()** - pushes a copy of the current drawing state onto the drawing state stack.
* **restore()** - pops the top entry in the drawing state stack, and resets the drawing state it describes. If there is no saved state, the method does nothing.

### Transformation

* **scale(x, y)** - adds the scaling transformation described by the arguments to the transformation matrix. The `x` argument represents the scale factor in the horizontal direction and the `y` argument represents the scale factor in the vertical direction. The factors are multiples.
* **rotate(angle)** - adds the rotation transformation described by the argument to the transformation matrix. The `angle` argument represents a clockwise rotation angle expressed in radians.
* **translate(x, y)** - adds the translation transformation described by the arguments to the transformation matrix. The `x` argument represents the translation distance in the horizontal direction and the `y` argument represents the translation distance in the vertical direction. The arguments are in coordinate space units.
* **transform(a, b, c, d, e, f)** - replaces the current transformation matrix with the result of multiplying the current transformation matrix with the matrix described by a, b, c, d, e, f arguments.

### Text

* **fillText(text, x, y, maxWidth)** - draws a stroked text to the current canvas. `fillStyle` is applied to the shapes and `strokeStyle` is ignored.
* **strokeText(text, x, y, maxWidth)** - draws a filled text to the current canvas. `strokeStyle` is applied to the shapes and `fillStyle` is ignored.
* **measureText(text)** - measures the current width of the specified text.

### Rectangles

* **clearRect(x, y, w, h)** - clears the pixels in the specified rectangle that also intersect the current clipping region. If either height or width are zero, this method has no effect.
* **fillRect(x, y, w, h)** - paints the specified rectangular area using the `fillStyle`. If either height or width are zero, this method has no effect.
* **strokeRect(x, y, w, h)** - draws a path which has a single subpath consisting of four points, with coordinates (x, y), (x+w, y), (x+w, y+h), and (x, y+h), connected to each other in that order by straight lines.

## References

1. <a href="https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_usage" target="_blank">https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_usage</a>
2. <a href="https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D" target="_blank">https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D</a>
3. <a href="https://www.w3.org/2015/04/2dcontext-lc-sample.html" target="_blank">https://www.w3.org/2015/04/2dcontext-lc-sample.html</a>
4. <a href="https://www.w3resource.com/html5-canvas/" target="_blank">https://www.w3resource.com/html5-canvas/</a>