---
title: "Html5 Canvas Introduction"
permalink: /learning/html5-canvas-introduction/
excerpt: "Html5 canvas introduction page."
description: "Html5 canvas introduction page."
last_modified_at: 2021-09-19T19:55:15+03:00
toc: true
---

## Introduction

HTML5 Canvas is a drawing technology that runs on all modern web browsers. It can be used for drawing shapes, photo manipulation, real-time video processing, data visualization, building games and animations.

HTML5 Canvas allows you to draw anything you want directly in the web browser without the use of plugins like Flash or Java.

## Canvas Element

HTML5 Canvas API is a 2D drawing API. The browser gives you a rectangular area on the html page and you can draw into this area using the canvas API. You can draw lines, shapes, images, text.

The Canvas API provides an interface for drawing graphics via JavaScript and the HTML `<canvas>` element.

The `<canvas>` element has only two attributes that are **width** and **height**. If width and height attributes are not set, the canvas width will be 300 pixels wide and the height 150 pixels.

### Falback Content

The `<canvas>` element supports fallback content, in order to be displayed in older browsers that does not support it. Versions of Internet Explorer earlier than version 9 or textual browsers does not support the `<canvas>` element. Fallback content should be set to be displayed by those browsers.

Fallback content added by inserting the a content inside the `<canvas>` element. Browsers that does not support the `<canvas>` element will render only the fallback content inside it. Browsers that support `<canvas>` will render the canvas element instead of the fallback content.

Here is an example of HTML code that creates Canvas element in the HTML page:

```html
<canvas id="canvas" width="300" height="300">
Your browser does not support HTML5 Canvas.
</canvas>
```

## Canvas Example

You can draw on the canvas using JavaScript:

### Code

```html
<html> 
<body> 
<canvas width="300" height="300" id="canvas"></canvas> 
<script> 
var canvas = document.getElementById('canvas'); 
var context = canvas.getContext('2d'); 
context.fillStyle = "red"; 
context.fillRect(50, 50, 200, 200); 
</script> 
</body> 
</html> 
```

### Result

<iframe class="code-sample-frame" frameborder="0" height="360" width="100%" id="frame_Basic_example" src="/html5-canvas-examples/assets/codes/learning/01-introduction-canvas-element.html" ></iframe>

## References

1. <a href="https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API" target="_blank">https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API</a>
2. <a href="https://en.wikipedia.org/wiki/Canvas_element" target="_blank">https://en.wikipedia.org/wiki/Canvas_element</a>
3. <a href="https://joshondesign.com/p/books/canvasdeepdive/title.html" target="_blank">https://joshondesign.com/p/books/canvasdeepdive/title.html</a>
4. <a href="https://www.w3.org/TR/2009/WD-html5-20090825/the-canvas-element.html" target="_blank">https://www.w3.org/TR/2009/WD-html5-20090825/the-canvas-element.html</a>
5. <a href="http://bucephalus.org/text/CanvasHandbook/CanvasHandbook.html" target="_blank">http://bucephalus.org/text/CanvasHandbook/CanvasHandbook.html</a>